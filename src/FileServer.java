import java.io.BufferedReader;
import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;


public class FileServer {

	public static void main(String[] args) {
		ServerSocket ss = null;

		try {
			ss = new ServerSocket(8888);
			
			System.out.println("Server socket created on port 8888");
			
			while(true) {
				Socket socket = null;
				PrintStream ps = null;
				BufferedReader br = null;

				try {
					socket = ss.accept();
					
					System.out.println("Client connected");
					ps = new PrintStream(socket.getOutputStream());				
					br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					
					String line = br.readLine();
					
					if(line == null) {
						// client closed the connection
						throw new IOException("Client closed the connection");
					}
					
					String[] tokens = line.split("\\s+");
					
					String fn = tokens[1];
					
					String emptyLine = br.readLine();
					if(emptyLine == null) {
						// client closed the connection
						throw new IOException("Client closed the connection");
					}
					
					System.out.println("Client requested file "+fn);
					
					BufferedReader fbr = null;
					try {
						fbr = new BufferedReader(new FileReader(fn));
						

						System.out.println("Reading file and sending it to client");
						while( (line = fbr.readLine()) != null) {
							ps.println(line);
						}
					} catch (FileNotFoundException fne) {
						System.out.println("File not found");
						ps.println("File not found");
					} finally {
						closeSilently(fbr);
						System.out.println("File closed");
					}
					
				} catch( IOException e) {
					System.out.println("IO Exception caught");
				} finally {
					closeSilently(ps); // make sure you close the output first
					closeSilently(br);
					closeSilently(socket);
					System.out.println("Client disconnected");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			closeSilently(ss);
		}
	}

	private static void closeSilently(Closeable c) {
		
		if(c!=null)
			try {
				c.close();
			} catch(IOException e) {}
	}

}
